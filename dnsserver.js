var dns = require('native-dns'),
	server = dns.createServer();

var totalRequests = 0;

server.on('request', function (request, response) {
	
	//console.log(request);
	
	//totalRequests++;
	//console.log('requests: ' + totalRequests);

	response.answer.push(dns.MX({
		name: request.question[0].name,
		priority: 10,
		exchange: 'smtp.miodominio.com',
		ttl: 600,
	}));

/*
	response.answer.push(dns.A({
		name: request.question[0].name,
		address: '127.0.0.1',
		ttl: 600,
	}));

	response.answer.push(dns.A({
		name: request.question[0].name,
		address: '127.0.0.2',
		ttl: 600,
	}));

	response.additional.push(dns.A({
		name: 'hostA.example.org',
		address: '127.0.0.3',
		ttl: 600,
	}));
*/

	response.send();
});

server.on('error', function (err, buff, req, res) {
	console.log(err.stack);
});

var ip = '10.0.0.240';
var port = 53;

server.serve(port, ip);
console.log('DNS Server listening on ' + ip + ':' + port);
